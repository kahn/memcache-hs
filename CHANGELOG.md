# 0.1.0.1 - February 26th, 2016

* Consistent usage of 'memcached' instead of 'memcache'.
* Document `Database.Memcache.Client`
* Add inline pragmas in appropriate places.
* Fix bug handling fragmented IP packets (Alfredo Di Napoli)

# 0.1.0.0 - May 18th, 2015

* First proper release (although still lots of rough edges!)
* Filled out `Data.Memcache.Client` to a complete API.
* Integrated cluster and authentication handling.
* Better error and exception handling.
* Fix compilation under GHC 7.10.

# 0.0.1 - May 5th, 2015

* Initial (incomplete) support for a cluster of memcached servers.
* Fixed compilation under GHC 7.4.

# 0.0.0 - August 23rd, 2013

* Initial release. Support for a single server.

